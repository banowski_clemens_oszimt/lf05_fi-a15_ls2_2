
import java.util.Scanner;

public class Rechner {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
        int zahl1 = scanner.nextInt();

        System.out.print("Gib einen Rechenoperator ein ( + | - | * | / ): ");
        String operator = scanner.next();

        System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
        int zahl2 = scanner.nextInt();

        scanner.close();

        int ergebnis;
        switch (operator) {
            case "+": ergebnis = zahl1 + zahl2; break;
            case "-": ergebnis = zahl1 - zahl2; break;
            case "*": ergebnis = zahl1 * zahl2; break;
            case "/": ergebnis = zahl1 / zahl2; break;
            default: ergebnis = -1;
        }

        System.out.print("\n\nErgebnis der Addition lautet: ");
        System.out.print(zahl1 + " " + operator + " " + zahl2 + " = " + ergebnis);
    }
}
